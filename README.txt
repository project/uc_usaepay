======================================
USAePay Payment Processor for Ubercart
======================================

USAePay (www.usaepay.com) is a payment processor based in the U.S.

Requirements: PHP 5+ and the PHP SOAP library (php.net/soap)

To get it up and running you'll need an account with USAePay and then click on
Settings -> Source Keys -> Add Source

Create a new Source Key for Ubercart (and a Pin if you want added security).
Enable the module in Drupal and navigate to
Store Administration -> Configuration -> Edit -> Payment Gateways

Copy in your Source Key under the USAePay settings and you should be good to go.

======================================
Recurring Billing
======================================

This module supports recurring billing by implementing the hooks from the
uc_recurring module.  To get recurring billing working, first make sure that
the "Customers & Billing" section is enabled in your USAePay account.  Next,
install the uc_recurring module, configure it to use USAePay as a fee handler,
and create a product with the recurring feature enabled.

Note that even though USAePay supports hosted recurring billing (i.e. it will
automatically charge the customer) we only use USAePay to securely store the
customer's credit card data and instead rely upon the uc_recurring module to
charge the customer on a recurring basis.  This allows the bulk of the
administration of recurring fees to happen through your Drupal site.

