<?php

/**
 * @file
 * UC recurring implementation for the UC USAePay gateway module.
 */

/**
 * Implementation of hook_recurring_info().
 */
function uc_usaepay_recurring_info() {
  $items['usaepay'] = array(
    'name' => t('USAePay'),
    'payment method' => 'credit',
    'module' => 'uc_usaepay',
    'fee handler' => 'usaepay',
    'renew callback' => 'uc_usaepay_recurring_renew',
    'process callback' => 'uc_usaepay_recurring_process',
    'menu' => array(
      'charge' => UC_RECURRING_MENU_DEFAULT,
      'edit'   => UC_RECURRING_MENU_DEFAULT,
      'update' => array(
        'title' => 'Update Account Details', 
        'page arguments' => array('uc_usaepay_recurring_update_form'),
        'file' => 'uc_usaepay.recurring.inc',
      ),
      'profile' => array(
        'title' => 'Customer Profile',
        'page arguments' => array('uc_usaepay_recurring_profile_form'),
        'access callback' => 'user_access',
        'access arguments' => array('administer recurring fees'),
        'file' => 'uc_usaepay.recurring.inc',
      ),
      'cancel' => UC_RECURRING_MENU_DEFAULT,
    ), // Use the default user operation defined in uc_recurring.
  );
  return $items;
}

/**
 * Set up the recurring fee by creating a customer profile for future payments
 *
 * @param $order
 *   The order object.
 * @param $fee
 *   The fee object.
 * @return
 *   TRUE if recurring fee setup
 */
function uc_usaepay_recurring_process($order, &$fee) {
  if (variable_get('uc_usaepay_create_customer', FALSE) == FALSE) {
    $data = array(
      'txn_type' => UC_CREDIT_REFERENCE_SET,
    );
    return uc_usaepay_charge($order->order_id, $order->total_amount, $data);
  }
  return TRUE;
}

/**
 * Process a renewal using the customer profile.
 *
 * @param $order
 *   The order object.
 * @param $fee
 *   The fee object.
 * @return
 *   TRUE if renewal succeeded
 */
function uc_usaepay_recurring_renew($order, &$fee) {
  if (!empty($order->data['cc_txns']['references'])) {
    $data = array(
      'txn_type' => UC_CREDIT_REFERENCE_TXN,
      'ref_id' => end(array_keys($order->data['cc_txns']['references'])),
    );
    $result = uc_usaepay_charge($order->order_id, $order->order_total, $data);

    if ($result['success'] == TRUE) {
      uc_payment_enter($order->order_id, $order->payment_method, $order->order_total, $fee->uid, $result['data'], $result['comment']);
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Create form for updating credit card details for recurring fee.
 */
function uc_usaepay_recurring_update_form($form_state, $rfid) {
  $form['rfid'] = array(
    '#type' => 'value',
    '#value' => $rfid,
  );
  $form['cc_data'] = array(
    '#type' => 'fieldset',
    '#title' => t('Credit card details'),
    '#theme' => 'uc_payment_method_credit_form',
    '#tree' => TRUE,
  );
  $form['cc_data'] += uc_payment_method_credit_form(array(), $order);
  unset($form['cc_data']['cc_policy']);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
    '#suffix' => l(t('Cancel'), 'user/'. $user->uid),
  );

  return $form;
}

/**
 * Implements update form submit for the authorizenet CIM gateway.
 */
function uc_usaepay_recurring_update_form_submit(&$form, &$form_state) {
  $fee = uc_recurring_fee_user_load($form_state['values']['rfid']);
  $order = uc_order_load($fee->order_id);
  $order->payment_details = $form_state['values']['cc_data'];
  $profile_id = end(array_keys($order->data['cc_txns']['references']));
  if ($message = _uc_usaepay_update_customer_cc_data($order, $profile_id)) {
    drupal_set_message('Account update failed.', 'error');
  }
  else {
    drupal_set_message('Account updated.');
    $form_state['redirect'] = 'user/'. $form_state['values']['uid'];
  }
}

/**
 * List the customer profile information.
 */
function uc_usaepay_recurring_profile_form($form_state, $rfid) {
  $fee = uc_recurring_fee_user_load($rfid);
  $order = uc_order_load($fee->order_id);
  $ref_id = end(array_keys($order->data['cc_txns']['references']));
  try {
    $client = _uc_usaepay_get_soap_client();
    $customer = $client->getCustomer(_uc_usaepay_get_token(), $ref_id);
  } catch (SoapFault $e) {
    watchdog('uc_usaepay', 'SOAP Error when fetching customer: @error', array('@error' => $e->getMessage()), WATCHDOG_ERROR);
    return $e->getMessage();
  }

  $customer = (array) $customer;
  foreach ($customer as $key => $val) {
    if (!is_string($val)) {
      $str = '';
      $val = (array) $val;
      foreach ($val as $k => $v) {
        if (is_string($v)) {
          $str .= "$k: $v<br/>";
        }
      }
      $customer[$key] = $str;
    }
  }
  $header = array_keys($customer);
  $rows[] = array_values($customer);

  $form['info'] = array(
    '#value' => t('The customer details below were return from USAePay'),
  );
  $form['profile'] = array(
    '#value' => theme('table', $header, $rows),
  );

  return $form;
}
